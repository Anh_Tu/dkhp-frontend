import React from 'react'

import Header from '../../../components/Header'

const SettingProfile = () => {
  return (
    <>
      <Header />
      {/* Profle Content */}
      <div className='col-lg-9'>
        {/* Nav tabs */}
        <ul className='nav nav-justified u-nav-v1-1 u-nav-primary g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20' role='tablist' data-target='nav-1-1-default-hor-left-underline' data-tabs-mobile-type='slide-up-down' data-btn-classes='btn btn-md btn-block rounded-0 u-btn-outline-primary g-mb-20'>
          <li className='nav-item'>
            <a className='nav-link g-py-10 active' data-toggle='tab' href='#nav-1-1-default-hor-left-underline--1' role='tab'>Edit Profile</a>
          </li>
        </ul>
        {/* End Nav tabs */}
        {/* Tab panes */}
        <div id='nav-1-1-default-hor-left-underline' className='tab-content'>
          {/* Edit Profile */}
          <div className='tab-pane fade show active' id='nav-1-1-default-hor-left-underline--1' role='tabpanel' data-parent='#nav-1-1-default-hor-left-underline'>
            <h2 className='h4 g-font-weight-300'>Manage your Name, ID and Email Addresses</h2>
            <p>Below are name, email addresse, contacts and more on file for your account.</p>
            <ul className='list-unstyled g-mb-30'>
              {/* Name */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Name</strong>
                  <span className='align-top'>John Doe</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Name */}
              {/* Your ID */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Your ID</strong>
                  <span className='align-top'>FKJ-032440</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Your ID */}
              {/* Company Name */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Company name</strong>
                  <span className='align-top'>Htmlstream</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Company Name */}
              {/* Position */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Position</strong>
                  <span className='align-top'>Project Manager</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Position */}
              {/* Primary Email Address */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Primary email address</strong>
                  <span className='align-top'>john.doe@htmlstream.com</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Primary Email Address */}
              {/* Linked Account */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Linked account</strong>
                  <span className='align-top'>Facebook</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Linked Account */}
              {/* Website */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Website</strong>
                  <span className='align-top'>https://htmlstream.com</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Website */}
              {/* Phone Number */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Phone number</strong>
                  <span className='align-top'>(+123) 456 7890</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Phone Number */}
              {/* Office Number */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Office number</strong>
                  <span className='align-top'>(+123) 456 7891</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Office Number */}
              {/* Address */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Address</strong>
                  <span className='align-top'>795 Folsom Ave, Suite 600, San Francisco CA, US </span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Address */}
            </ul>
            <div className='text-sm-right'>
              <a className='btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10' href='#'>Cancel</a>
              <a className='btn u-btn-primary rounded-0 g-py-12 g-px-25' href='#'>Save Changes</a>
            </div>
          </div>
          {/* End Edit Profile */}
        </div>
        {/* End Tab panes */}
      </div>
      {/* End Profle Content */}
    </>
  )
}

export default SettingProfile
