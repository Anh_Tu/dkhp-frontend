import React from 'react'

import Header from '../../components/Header'
import PricingInfo from '../../containers/PricingInfo'
import Question from '../../containers/Questions'
import Process from '../../containers/Process'

const Pricing = () => {
  return (
    <>
      <Header />
      <Process />
      <PricingInfo />
      <Question />
    </>
  )
}

export default Pricing
