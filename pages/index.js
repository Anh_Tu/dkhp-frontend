import React, { useState, useEffect } from 'react'
import Axios from 'axios'
import redirect from '../utils/redirect'
import Router from 'next/router'
import Swal from 'sweetalert2'

const API_URL = 'http://13.229.230.229/auth/local'

const auth = () => {
  const [identifier, setIdentifier] = useState('')
  const [password, setPassword] = useState('')
  const [jwt, setJwt] = useState('')
  const [isLogin, setIsLogin] = useState(false)
  const [id, setId] = useState('')

  const handleSubmit = (event) => {
    event.preventDefault()
    Axios.post(API_URL, {
      identifier,
      password
    }).then(({ data }) => {
      if (data) {
        setId(data.user._id)
        let timerInterval
        Swal.fire({
          title: 'Đang đăng nhập...',
          timer: 1000,
          icon: 'success',
          timerProgressBar: true,
          onBeforeOpen: () => {
            Swal.showLoading()
            timerInterval = setInterval(() => {
              const content = Swal.getContent()
              if (content) {
                const b = content.querySelector('b')
                if (b) {
                  b.textContent = Swal.getTimerLeft()
                }
              }
            }, 100)
          },
          onClose: () => {
            clearInterval(timerInterval)
          }
        }).then((result) => {
          /* Read more about handling dismissals below */
          if (result.dismiss === Swal.DismissReason.timer) {
            setJwt(data.jwt)
          }
        })
      }
    })
      .catch(err => {
        if (err) {
          Swal.fire({
            icon: 'error',
            title: 'Đăng nhập không thành công',
            text: 'Xin hãy kiểm tra MSSV & Mật khẩu'
          })
        }
      })
  }

  const getInitialProps = (context) => {
    if (auth.isLogin) {
      redirect(context, '/student/profile')
    }
    return {}
  }

  useEffect(() => {
    if (jwt !== '') {
      setIsLogin(true)
      Router.push('/student/profile')
      getInitialProps()
    }
    setIsLogin(false)
  }, [jwt])

  return (
    <main>
      <section className='g-min-height-100vh g-flex-centered g-bg-lightblue-radialgradient-circle'>
        <div className='container g-py-100'>
          <div className='row justify-content-center'>
            <div className='col-sm-8 col-lg-5'>
              <div className='u-shadow-v24 g-bg-white rounded g-py-40 g-px-30'>
                <header className='text-center mb-4'>
                  <h2 className='h2 g-color-black g-font-weight-600'>Đăng nhập</h2>
                </header>

                {/* <!-- Form --> */}
                <form className='g-py-15'>
                  <div className='mb-4'>
                    <label className='g-color-gray-dark-v2 g-font-weight-600 g-font-size-13'>Mã số sinh viên:</label>
                    <input
                      className='form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-blue--hover rounded g-py-15 g-px-15' type='email' placeholder='johndoe@gmail.com'
                      onChange={(event) => setIdentifier(event.target.value)}
                    />
                  </div>

                  <div className='g-mb-35'>
                    <div className='row justify-content-between'>
                      <div className='col align-self-center'>
                        <label className='g-color-gray-dark-v2 g-font-weight-600 g-font-size-13'>Mật khẩu:</label>
                      </div>
                      <div className='col align-self-center text-right'>
                        <a className='d-inline-block g-font-size-12 mb-2' href='#'>Quên mật khẩu ư?</a>
                      </div>
                    </div>
                    <input
                      className='form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-blue--hover rounded g-py-15 g-px-15 mb-3' type='password' placeholder='Password'
                      onChange={(event) => setPassword(event.target.value)}
                    />
                    <div className='row justify-content-between'>
                      <div className='col-8 align-self-center'>
                        <label className='form-check-inline u-check g-color-gray-dark-v5 g-font-size-12 g-pl-25 mb-0'>
                          <input className='g-hidden-xs-up g-pos-abs g-top-0 g-left-0' type='checkbox' />
                          <div className='u-check-icon-checkbox-v6 g-absolute-centered--y g-left-0'>
                            <i className='fa' data-check-icon='x' />
                          </div>
                        Ghi nhớ tôi
                        </label>
                      </div>
                      <div className='col-4 align-self-center text-right'>
                        <button
                          className='btn btn-md u-btn-blue rounded g-py-13 g-px-10' type='button'
                          onClick={handleSubmit}
                        >Đăng nhập
                        </button>
                      </div>
                    </div>
                  </div>
                </form>
                {/* <!-- End Form --> */}

                {/* <footer className='text-center'>
                  <p className='g-color-gray-dark-v5 g-font-size-13 mb-0'>Don't have an account? <a className='g-font-weight-600' href='page-signup-6.html'>signup</a>
                  </p>
                </footer> */}
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
  )
}

// auth.getInitialProps = (context) => {
//   if (auth.isLogin) {
//     redirect(context, '/student/profile')
//   }
//   return {}
// }

export default auth
