import Document, { Html, Head, Main, NextScript } from 'next/document'
import flush from 'styled-jsx/server'
// import $ from 'jquery'

class MyDocument extends Document {
  static async getInitialProps ({ renderPage }) {
    const { html, head, errorHtml, chunks } = renderPage()
    const styles = flush()

    return { html, head, errorHtml, chunks, styles }
  }

  render () {
    return (
      <Html>
        <Head>
          {/* <!-- Required Meta Tags Always Come First --> */}
          <meta charset='utf-8' />
          <meta name='viewport' content='width=device-width, initial-scale=1, shrink-to-fit=no' />
          <meta http-equiv='x-ua-compatible' content='ie=edge' />

          <title>UIT - Đăng kí học phần</title>

          {/* <!-- Google Fonts --> */}
          <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800' />

          {/* <!-- CSS Global Compulsory --> */}
          <link rel='stylesheet' href='/assets/vendor/bootstrap/bootstrap.min.css' />
          <link rel='stylesheet' href='/assets/vendor/icon-awesome/css/font-awesome.min.css' />
          <link rel='stylesheet' href='/assets/vendor/icon-line-pro/style.css' />
          <link rel='stylesheet' href='/assets/vendor/animate.css' />

          {/* <!-- CSS Global Icons --> */}
          <link rel='stylesheet' href='/assets/vendor/icon-line/css/simple-line-icons.css' />
          <link rel='stylesheet' href='/assets/vendor/icon-etlinefont/style.css' />
          <link rel='stylesheet' href='/assets/vendor/icon-hs/style.css' />
          <link rel='stylesheet' href='/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.min.css' />
          <link rel='stylesheet' href='/assets/vendor/hs-megamenu/src/hs.megamenu.css' />
          <link rel='stylesheet' href='/assets/vendor/hamburgers/hamburgers.min.css' />

          {/* <!-- CSS Unify --> */}
          <link rel='stylesheet' href='/assets/css/unify-core.css' />
          <link rel='stylesheet' href='/assets/css/unify-components.css' />
          <link rel='stylesheet' href='/assets/css/unify-globals.css' />

          {/* <!-- CSS Customization --> */}
          <link rel='stylesheet' href='/assets/css/custom.css' />

          <div className='u-outer-spaces-helper' />

          {/* <!-- JS Global Compulsory --> */}
          <script src='/assets/vendor/jquery/jquery.min.js' />
          <script src='/assets/vendor/jquery-migrate/jquery-migrate.min.js' />
          <script src='/assets/vendor/popper.js/popper.min.js' />
          <script src='/assets/vendor/bootstrap/bootstrap.min.js' />

          {/* <!-- JS Implementing Plugins --> */}
          <script src='/assets/vendor/appear.js' />
          <script src='/assets/vendor/hs-megamenu/src/hs.megamenu.js' />
          <script src='/assets/vendor/circles/circles.min.js' />
          <script src='/assets/vendor/malihu-scrollbar/jquery.mCustomScrollbar.concat.min.js' />

          {/* <!-- JS Unify --> */}
          <script src='/assets/js/hs.core.js' />
          <script src='/assets/js/helpers/hs.hamburgers.js' />
          <script src='/assets/js/components/hs.header.js' />
          <script src='/assets/js/components/hs.tabs.js' />
          <script src='/assets/js/components/hs.progress-bar.js' />
          <script src='/assets/js/components/hs.scrollbar.js' />
          <script src='/assets/js/components/hs.chart-pie.js' />
          <script src='/assets/js/components/hs.go-to.js' />

          {/* <!-- JS Customization --> */}
          <script src='/assets/js/custom.js' />
          {/* <script src='/static/jquery.js' type='text/javascript' /> */}

          <a
            className='js-go-to u-go-to-v1' href='#' data-type='fixed' data-position='{
              "bottom": 15,
              "right": 15
            }' data-offset-top='400' data-compensation='#js-header' data-show-effect='zoomIn'
          >
            <i className='hs-icon hs-icon-arrow-top' />
          </a>

          <script
            type='text/javascript' dangerouslySetInnerHTML={{
              __html: `
          $(document).on('ready', function () {
            // initialization of go to
            $.HSCore.components.HSGoTo.init('.js-go-to');
    
            // initialization of tabs
            $.HSCore.components.HSTabs.init('[role="tablist"]');
    
            // initialization of chart pies
            var items = $.HSCore.components.HSChartPie.init('.js-pie');
    
            // initialization of HSScrollBar component
            $.HSCore.components.HSScrollBar.init( $('.js-scrollbar') );
          });
    
          $(window).on('load', function () {
            // initialization of header
            $.HSCore.components.HSHeader.init($('#js-header'));
            $.HSCore.helpers.HSHamburgers.init('.hamburger');
    
            // initialization of HSMegaMenu component
            $('.js-mega-menu').HSMegaMenu({
              event: 'hover',
              pageContainer: $('.container'),
              breakpoint: 991
            });
    
            // initialization of horizontal progress bars
            setTimeout(function () { // important in this case
              var horizontalProgressBars = $.HSCore.components.HSProgressBar.init('.js-hr-progress-bar', {
                direction: 'horizontal',
                indicatorSelector: '.js-hr-progress-bar-indicator'
              });
            }, 1);
          });
    
          $(window).on('resize', function () {
            setTimeout(function () {
              $.HSCore.components.HSTabs.init('[role="tablist"]');
            }, 200);
          });
          `
            }}
          />

        </Head>
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

export default MyDocument
