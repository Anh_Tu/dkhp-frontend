// import { routerReplace } from '../router'

export default (context, target) => {
  if (context.res) {
    context.res.writeHead(303, { Location: typeof target === 'string' ? target : (target?.as || target?.href || '/') })
    context.res.end()
  } else {
    // In the browser, we just pretend like this never even happened ;)
    // Router.replace(target)
    // routerReplace(target)
  }
}
