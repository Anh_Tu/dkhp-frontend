import { Router } from '../i18n'

export const routerPush = (router, ...args) => {
  window.scrollTo(0, 0)

  if (typeof router === 'string') {
    Router.push(router, ...args)
    return
  }

  Router.push(router.href, router.as, ...args)
}

export const routerReplace = (router, ...args) => {
  if (typeof router === 'string') {
    Router.push(router, ...args)
    return
  }

  Router.replace(router.href, router.as, ...args)
}

export default Router
