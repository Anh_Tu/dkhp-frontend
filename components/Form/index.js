import React from 'react'

const Form = (props) => {
  return (
    <>
      {/* Contact Form */}
      <div className='container text-center'>
        <header className='u-heading-v8-2 text-center g-width-70x--md mx-auto g-mb-60'>
          <h2 className='u-heading-v8__title text-uppercase g-font-weight-600 g-mb-25'>Đăng kí môn học</h2>
          <p className='lead mb-0'>
            Dán mã lớp bạn muốn đăng kí vào form bên dưới. Hệ thống sẽ trả về kết quả trong giây lát.
          </p>
        </header>
        {/* Contact Form */}
        <div className='row justify-content-center'>
          <div className='col-md-8'>
            <form>
              <div className='row'>
                <div className='col-md-12 form-group g-mb-30'>
                  <textarea className='form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-0 g-py-13 g-px-15' rows={7} placeholder='Danh sách mã lớp.' defaultValue='' />
                </div>
                <div className='col-md-12 form-group g-mb-30'>
                  <input className='form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-0 g-py-13 g-px-15' type='text' placeholder='Ghi chú thêm cho thầy cô bộ môn' />
                </div>
              </div>
              <button className='btn u-btn-primary rounded-0 g-py-12 g-px-20' type='submit' role='button'>Gửi đi</button>
            </form>
          </div>
        </div>
        {/* Contact Form */}
      </div>
      {/* End Contact Form */}
    </>
  )
}

export default Form
