import React from 'react'

const Subject = (props) => {
  return (
    <>
      {/* Popular Jobs */}
      <section className='g-py-100'>
        <div className='container'>
          <header className='text-center g-width-60x--md mx-auto g-mb-50'>
            <h2 className='h1 g-color-gray-dark-v1 g-font-weight-300'>Tất cả các môn khả dụng</h2>
            <p className='lead'>
              Bạn chỉ có thể đăng kí những lớp còn trống chỗ. Và hãy lưu ý về số tín chỉ đã đăng kí.
            </p>
          </header>
          <div className='row g-mb-30'>
            <div className='col-lg-4 g-mb-40 g-mb-0--lg'>
              <ul className='list-unstyled mb-0'>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img1.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img2.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img3.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img4.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
              </ul>
            </div>
            <div className='col-lg-4 g-mb-40 g-mb-0--lg'>
              <ul className='list-unstyled mb-0'>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img5.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img6.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img5.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img4.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
              </ul>
            </div>
            <div className='col-lg-4'>
              <ul className='list-unstyled mb-0'>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img3.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img2.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img1.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
                <li className='media u-shadow-v11 rounded g-pa-20 g-mb-10'>
                  {/* <div className='d-flex align-self-center g-mt-3 g-mr-15'>
                    <img className='g-width-40 g-height-40' src='../../assets/img-temp/logos/img5.png' alt='Image Description' />
                  </div> */}
                  <div className='media-body'>
                    <a className='d-block u-link-v5 g-color-main g-color-primary--hover g-font-weight-600 g-mb-3' href>Nhập môn công nghệ phần mềm</a>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-location-pin g-pos-rel g-top-1 mr-1' /> Đỗ Thị Thanh Tuyền.
                    </span>
                    <span className='g-font-size-13 g-color-gray-dark-v4 g-mr-15'>
                      <i className='icon-directions g-pos-rel g-top-1 mr-1' /> Thứ 3.
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </div>
          <div className='text-center'>
            <a className='btn btn-xl u-btn-outline-primary text-uppercase g-font-weight-600 g-font-size-12' href='#'>Nhiều hơn...</a>
          </div>
        </div>
      </section>
      {/* End Popular Jobs */}
    </>
  )
}

export default Subject
