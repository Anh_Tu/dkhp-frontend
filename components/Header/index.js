import React from 'react'
import Router from 'next/router'

const Header = () => {
  const handleHome = () => {
    Router.push('/')
  }

  const handlePricing = () => {
    Router.push('/pricing')
  }

  const handleDKHP = () => {
    Router.push('/form-register')
  }

  return (
    <>
      {/* Header */}
      <header id='js-header' className='u-header u-header--static'>
        <div className='u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10'>
          <nav className='js-mega-menu navbar navbar-expand-lg hs-menu-initialized hs-menu-horizontal'>
            <div className='container'>
              {/* Responsive Toggle Button */}
              <button className='navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-minus-3 g-right-0' type='button' aria-label='Toggle navigation' aria-expanded='false' aria-controls='navBar' data-toggle='collapse' data-target='#navBar'>
                <span className='hamburger hamburger--slider'>
                  <span className='hamburger-box'>
                    <span className='hamburger-inner' />
                  </span>
                </span>
              </button>
              {/* End Responsive Toggle Button */}
              {/* Logo */}
              <a href='/index.html' className='navbar-brand d-flex' />
              {/* End Logo */}
              {/* Navigation */}
              <div className='collapse navbar-collapse align-items-center flex-sm-row g-pt-10 g-pt-5--lg g-mr-40--lg' id='navBar'>
                <ul className='navbar-nav text-uppercase g-pos-rel g-font-weight-600 ml-auto'>
                  {/* Home */}
                  <li className='hs-has-mega-menu nav-item  g-mx-10--lg g-mx-15--xl' data-animation-in='fadeIn' data-animation-out='fadeOut' data-max-width='60%' data-position='left'>
                    <a id='mega-menu-home' className='nav-link g-py-7 g-px-0' href='#' aria-haspopup='true' aria-expanded='false' onClick={handleHome}>Trang chủ
                      <i className='hs-icon hs-icon-arrow-bottom g-font-size-11 g-ml-7' />
                    </a>

                  </li>
                  {/* <!-- Intro --> */}
                  <li className='nav-item  g-mx-10--lg g-mx-15--xl'>
                    <a className='nav-link g-py-7 g-px-0' onClick={handlePricing} href='/pricing'>Học phí</a>
                  </li>
                  {/* <!-- End Intro --> */}
                  {/* End Home */}

                </ul>
              </div>
              {/* End Navigation */}
              <div className='d-inline-block g-hidden-md-down g-pos-rel g-valign-middle g-pl-30 g-pl-0--lg'>
                <a className='btn u-btn-outline-primary g-font-size-13 text-uppercase g-py-10 g-px-15' target='_blank' onClick={handleDKHP}>Đăng kí môn học</a>
              </div>
            </div>
          </nav>
        </div>
      </header>
      {/* End Header */}

      {/* Breadcrumb */}
      <section className='g-my-30'>
        <div className='container'>
          <ul className='u-list-inline'>
            <li className='list-inline-item g-mr-7'>
              <a className='u-link-v5 g-color-main g-color-primary--hover' href='#'>Home</a>
              <i className='fa fa-angle-right g-ml-7' />
            </li>
            <li className='list-inline-item g-mr-7'>
              <a className='u-link-v5 g-color-main g-color-primary--hover' href='#'>Student</a>
              <i className='fa fa-angle-right g-ml-7' />
            </li>
            <li className='list-inline-item g-color-primary'>
              <span>Profile</span>
            </li>
          </ul>
        </div>
      </section>
      {/* End Breadcrumb */}
    </>
  )
}

export default Header
