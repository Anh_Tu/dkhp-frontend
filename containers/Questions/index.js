import React from 'react'

const Question = () => {
  return (
    <>
      <section className='g-bg-secondary'>
        <div className='container g-pt-100 g-pb-40'>
          <div className='row justify-content-center g-mb-60'>
            <div className='col-lg-7'>
              {/* Heading */}
              <div className='text-center'>
                <h2 className='h3 g-color-black text-uppercase mb-2'>Câu hỏi thường gặp</h2>
                <div className='d-inline-block g-width-35 g-height-2 g-bg-primary mb-2' />
                <p className='lead mb-0'>
                  Sinh viên có thể để lại câu hỏi của mình để được giải đáp ngay bên dưới
                </p>
              </div>
              {/* End Heading */}
            </div>
          </div>
          <div id='accordion-12' className='u-accordion u-accordion-color-primary' role='tablist' aria-multiselectable='true'>
            {/* Card */}
            <div className='card g-brd-none rounded-0 g-mb-15'>
              <div id='accordion-12-heading-01' className='u-accordion__header g-pa-0' role='tab'>
                <h5 className='mb-0'>
                  <a className='d-flex g-color-main g-text-underline--none--hover g-brd-around g-brd-gray-light-v4 g-rounded-5 g-pa-10-15' href='#accordion-12-body-01' data-toggle='collapse' data-parent='#accordion-12' aria-expanded='true' aria-controls='accordion-12-body-01'>
                    <span className='u-accordion__control-icon g-mr-10'>
                      <i className='fa fa-angle-down' />
                      <i className='fa fa-angle-up' />
                    </span>
                    Học phí
                  </a>
                </h5>
              </div>
              <div id='accordion-12-body-01' className='collapse show' role='tabpanel' aria-labelledby='accordion-12-heading-01' data-parent='#accordion-12'>
                <div className='u-accordion__body g-color-gray-dark-v5'>
                Học phí dự kiến với sinh viên chính quy khóa tuyển năm 2020 (sau khi đề án đổi mới cơ chế hoạt động của Trường ĐHCNTT được phê duyệt). <a href='https://tuyensinh.uit.edu.vn/cau-hoi-thuong-gap'>Xem thêm</a>
                </div>
              </div>
            </div>
            {/* End Card */}
            {/* Card */}
            <div className='card g-brd-none rounded-0 g-mb-15'>
              <div id='accordion-12-heading-02' className='u-accordion__header g-pa-0' role='tab'>
                <h5 className='mb-0'>
                  <a className='collapsed d-flex g-color-main g-text-underline--none--hover g-brd-around g-brd-gray-light-v4 g-rounded-5 g-pa-10-15' href='#accordion-12-body-02' data-toggle='collapse' data-parent='#accordion-12' aria-expanded='false' aria-controls='accordion-12-body-02'>
                    <span className='u-accordion__control-icon g-mr-10'>
                      <i className='fa fa-angle-down' />
                      <i className='fa fa-angle-up' />
                    </span>
                    Các chương trình đào tạo đặc biệt
                  </a>
                </h5>
              </div>
              <div id='accordion-12-body-02' className='collapse' role='tabpanel' aria-labelledby='accordion-12-heading-02' data-parent='#accordion-12'>
                <div className='u-accordion__body g-color-gray-dark-v5'>
                  <p>Ngoài chương trình chính qui đại trà, Trường ĐH Công nghệ Thông tin còn có các chương trình đào tạo đặc biệt như sau. Dưới đây là phần giải thích ngắn về các chương trình đặc biệt.</p>
                  <br />
                  <i>a) Chương trình tài năng</i>
                  <p>Chương trình đào tạo dành cho sinh viên giỏi, học phí ngang mức học phí hệ chính qui đại trà. Tất cả sinh viên tài năng được nhận học bổng tài năng hằng tháng, mức học bổng tài năng hiện tại 500.000đ/ sinh viên/ tháng. Bằng cấp Kỹ sư Tài năng An toàn Thông tin , Cử nhân Tài năng Khoa học Máy tính do ĐH Công nghệ Thông tin cấp.
                Hiện có 02 ngành có đào tạo chương trình tài năng: Khoa học Máy tính, An toàn Thông tin.
                Chỉ tiêu mỗi ngành 30 sinh viên, xét tuyển dựa trên điêm thi THPT Quốc gia. Sinh viên sau khi đã trúng tuyển và nhập học vào trường có thể đăng ký xét tuyển vào chương trình tài năng.
                  </p>
                  <i>b) Chương trình tiên tiến</i>
                  <p>Chương trình đào tạo hoàn toàn bằng Tiếng Anh theo khung chương trình của ĐH Oklahoma, Hoa Kỳ. Bằng cấp Kỹ sư chương trình tiên tiến do ĐH Công nghệ Thông tin cấp.
Điều kiện học tập tốt nhất : qui mô lớp nhỏ ( 20-40 sinh viên/ lớp) ,cơ sở vật chất hiện đại, phòng học trang bị điều hòa, phòng thực hành máy tính cấu hình cao, giảng viên là các Tiến sĩ , Phó giáo sư, Giáo sư có nhiều năm kinh nghiệm giảng dạy của ĐH Quốc gia và các đại học nước ngoài có liên hệ với Trường .Các chương trình dã ngoại, lớp học kỹ năng mềm dành riêng cho sinh viên. Trong quá trình học nếu sinh viên có nhu cầu du học thì điểm các học phần đã học tại trường vẫn được trường đại học nước ngoài chấp nhận.
Ngành đào tạo : Hệ thống Thông tin.
Học phí của từng khóa sẽ được công bố cụ thể cho toàn khóa học và không thay đổi suốt khóa học.
                  </p>
                  <i>c) Chương trình chất lượng cao (CLCA)</i>
                  <p>Chương trình đào tạo theo khung chương trình hệ đào tạo chính qui đại trà. Bằng cấp đại học chính qui do Trường ĐH Công nghệ Thông tin cấp.
Ngôn ngữ giảng dạy trong chương trình là Tiếng Việt.
Môn học ngoại ngữ trong chương trình là Tiếng Anh. Sinh viên được tăng cường đào tạo môn Tiếng Anh với các lớp Tiếng Anh tăng cường
Điều kiện học tập tốt nhất: qui mô lớp nhỏ ( 20-40 sinh viên/ lớp) , cơ sở vật chất hiện đại, phòng học trang bị điều hòa, phòng thực hành máy tính cấu hình cao, giảng viên là các Thạc sĩ, Tiến sĩ có nhiều năm kinh nghiệm giảng dạy của ĐH Quốc gia.
Trong một giai đoạn chuyên ngành sẽ có một số môn học được giảng dạy bằng Tiếng Anh, (đối với chương trình định hướng Nhật Bản được giảng dạy bằng Tiếng Nhật)
Các chương trình dã ngoại, lớp học kỹ năng mềm dành riêng cho sinh viên.
Ngành đào tạo: Kỹ thuật Phần mềm, Kỹ thuật Máy tính, Hệ thống Thông tin, Khoa học Máy tính, Mạng máy tính và Truyền thông Dữ liệu, An toàn Thông tin, Thương mại Điện tử.
Học phí của từng khóa sẽ được công ḅố cụ thể cho toàn khóa học và không thay đổi suốt khóa học.
                  </p>
                  <i>d) Chương trình chất lượng cao Định hướng Nhật  Bản (CLCN)</i>
                  <p>Ngành đào tạo: Công nghệ Thông tin ( Mã ngành 7480201_CLCN)
Chương trình đào tạo theo khung chương trình hệ đào tạo chính qui đại trà. Bằng cấp đại học chính qui do Trường ĐH Công nghệ Thông tin cấp.
Ngôn ngữ giảng dạy trong chương trình là Tiếng Việt.
Môn học ngoại ngữ là Tiếng Nhật. Sinh viên tốt nghiệp phải đạt chuẩn đầu ra ngoại ngữ Tiếng Nhật JLPT trình độ N3.
Điều kiện học tập tốt nhất : qui mô lớp nhỏ ( 20-40 sinh viên/ lớp) , cơ sở vật chất hiện đại, phòng học trang bị điều hòa, phòng thực hành máy tính cấu hình cao, giảng viên là các Thạc sĩ, Tiến sĩ có nhiều năm kinh nghiệm giảng dạy của ĐH Quốc gia.
Trong một giai đoạn chuyên ngành sẽ có một số môn học được giảng dạy bằng Tiếng Nhật. Sinh viên được tăng cường giảng dạy về qui trình , văn hóa làm việc tại Nhật Bản. Sinh viên được giới thiệu thực tập tại Nhật Bản hoặc chi nhánh các công ty Nhật Bản tại Việt Nam.
Các chương trình dã ngoại, lớp học kỹ năng mềm dành riêng cho sinh viên.
Học phí của từng khóa sẽ được công ḅố cụ thể cho toàn khóa học và không thay đổi suốt khóa học.
                  </p>
                </div>
              </div>
            </div>
            {/* End Card */}
            {/* Card */}
            <div className='card g-brd-none rounded-0 g-mb-15'>
              <div id='accordion-12-heading-03' className='u-accordion__header g-pa-0' role='tab'>
                <h5 className='mb-0'>
                  <a className='collapsed d-flex g-color-main g-text-underline--none--hover g-brd-around g-brd-gray-light-v4 g-rounded-5 g-pa-10-15' href='#accordion-12-body-03' data-toggle='collapse' data-parent='#accordion-12' aria-expanded='false' aria-controls='accordion-12-body-03'>
                    <span className='u-accordion__control-icon g-mr-10'>
                      <i className='fa fa-angle-down' />
                      <i className='fa fa-angle-up' />
                    </span>
                    Điều kiện ăn ở, sinh hoạt?
                  </a>
                </h5>
              </div>
              <div id='accordion-12-body-03' className='collapse' role='tabpanel' aria-labelledby='accordion-12-heading-03' data-parent='#accordion-12'>
                <div className='u-accordion__body g-color-gray-dark-v5'>
                Ký túc xá ĐH Quốc gia nằm trong khu đô thi ĐH Quốc gia, cách trường khoảng 1 km, có sức chứa hơn 24.000 chỗ đủ chổ ở cho toàn bộ sinh viên của Trường.
Tân sinh viên chuẩn bị 01 Bản sao Giấy báo nhập học, 02 Bản sao Giấy CMND hoặc Thẻ căn cước
Lệ phí Ký túc xá HK1 năm học 2019-2020 theo thông báo sau:
                </div>
              </div>
            </div>
            {/* End Card */}
          </div>

        </div>
      </section>
      {/* End Icon Blocks */}

    </>
  )
}

export default Question
