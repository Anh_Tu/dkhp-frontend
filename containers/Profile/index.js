import React from 'react'

const Profile = (props) => {
  return (
    <>
      {/* <!-- Profle Content --> */}
      <div className='col-lg-9'>
        {/* <!-- User Block --> */}
        <div className='g-brd-around g-brd-gray-light-v4 g-pa-20 g-mb-40'>
          <div className='row'>
            <div className='col-lg-4 g-mb-40 g-mb-0--lg'>
              {/* <!-- User Image --> */}
              <div className='g-mb-20'>
                <img className='img-fluid w-100' src='https://i.imgur.com/4o1obwD.jpg' alt='Image Description' style={{ borderRadius: 20 }} />
              </div>
              {/* <!-- User Image --> */}

              {/* <!-- User Contact Buttons --> */}
              <a className='btn btn-block u-btn-outline-blue g-rounded-50 g-py-12 g-mb-10' onClick={props.handleDKHP}>
                    Đăng kí môn học
              </a>
              <a className='btn btn-block u-btn-darkgray g-rounded-50 g-py-12 g-mb-10' href='#' onClick={props.handlePricing}>
                    Học phí
              </a>
              {/* <!-- End User Contact Buttons --> */}
            </div>

            <div className='col-lg-8'>
              {/* <!-- User Details --> */}
              <div className='d-flex align-items-center justify-content-sm-between g-mb-5'>
                <h2 className='g-font-weight-300 g-mr-10'>Nguyễn Trần Hữu Đăng</h2>
              </div>
              {/* <!-- End User Details --> */}

              {/* <!-- User Position --> */}
              <h4 className='h6 g-font-weight-300 g-mb-10'>
                <i className='icon-badge g-pos-rel g-top-1 g-mr-5 g-color-gray-dark-v5' /> Khoa công nghệ phần mềm.
              </h4>
              {/* <!-- End User Position --> */}

              {/* <!-- User Info --> */}
              <ul className='list-inline g-font-weight-300'>
                <li className='list-inline-item g-mr-20'>
                  <i className='icon-location-pin g-pos-rel g-top-1 g-color-gray-dark-v5 g-mr-5' /> Kĩ thuật phầm mềm
                </li>
                <li className='list-inline-item g-mr-20'>
                  <i className='icon-check g-pos-rel g-top-1 g-color-gray-dark-v5 g-mr-5' /> Khoá 13
                </li>
              </ul>
              {/* <!-- End User Info --> */}

              <hr className='g-brd-gray-light-v4 g-my-20' />

              <p className='lead g-line-height-1_8'>
                    Sinh viên hiện đang học bình thường với số tính chỉ tích luỹ là 43
              </p>

              <hr className='g-brd-gray-light-v4 g-my-20' />

              {/* <!-- User Skills --> */}
              <div className='d-flex flex-wrap text-center'>
                {/* <!-- Counter Pie Chart --> */}
                <div className='g-mr-40 g-mb-20 g-mb-0--xl'>
                  <div
                    className='js-pie g-color-purple g-mb-5' data-circles-value='54' data-circles-max-value='100' data-circles-bg-color='#d3b6c6' data-circles-fg-color='#9b6bcc' data-circles-radius='30' data-circles-stroke-width='3' data-circles-additional-text='%' data-circles-duration='2000'
                    data-circles-scroll-animate='true' data-circles-font-size='14'
                  />
                  <h4 className='h6 g-font-weight-300'>Phương pháp mô hình hoá</h4>
                </div>
                {/* <!-- End Counter Pie Chart --> */}

                {/* <!-- Counter Pie Chart --> */}
                <div className='g-mr-40 g-mb-20 g-mb-0--xl'>
                  <div
                    className='js-pie g-color-blue g-mb-5' data-circles-value='72' data-circles-max-value='100' data-circles-bg-color='#bee3f7' data-circles-fg-color='#3498db' data-circles-radius='30' data-circles-stroke-width='3' data-circles-additional-text='%' data-circles-duration='2000'
                    data-circles-scroll-animate='true' data-circles-font-size='14'
                  />
                  <h4 className='h6 g-font-weight-300'>Tư tưởng Hồ Chí Minh</h4>
                </div>
                {/* <!-- End Counter Pie Chart --> */}

                {/* <!-- Counter Pie Chart --> */}
                <div className='g-mr-40 g-mb-20 g-mb-0--xl'>
                  <div
                    className='js-pie g-color-lightred g-mb-5' data-circles-value='81' data-circles-max-value='100' data-circles-bg-color='#ffc2bb' data-circles-fg-color='#e74c3c' data-circles-radius='30' data-circles-stroke-width='3' data-circles-additional-text='%' data-circles-duration='2000'
                    data-circles-scroll-animate='true' data-circles-font-size='14'
                  />
                  <h4 className='h6 g-font-weight-300'>Nhập môn công nghệ phần mềm</h4>
                </div>
                {/* <!-- End Counter Pie Chart --> */}

                {/* <!-- Counter Pie Chart --> */}
                <div className='g-mr-40 g-mb-20 g-mb-0--xl'>
                  <div
                    className='js-pie g-color-blue g-mb-5' data-circles-value='83' data-circles-max-value='100' data-circles-bg-color='#c9ff97' data-circles-fg-color='#72c02c' data-circles-radius='30' data-circles-stroke-width='3' data-circles-additional-text='%' data-circles-duration='2000'
                    data-circles-scroll-animate='true' data-circles-font-size='14'
                  />
                  <h4 className='h6 g-font-weight-300'>Đặc tả hình thức</h4>
                </div>
                {/* <!-- End Counter Pie Chart --> */}

                {/* <!-- Counter Pie Chart --> */}
                <div className='g-mb-20 g-mb-0--lg'>
                  <div
                    className='js-pie g-mb-5' data-circles-value='92' data-circles-max-value='100' data-circles-bg-color='#eeeeee' data-circles-fg-color='#111111' data-circles-radius='30' data-circles-stroke-width='3' data-circles-additional-text='%' data-circles-duration='2000'
                    data-circles-scroll-animate='true' data-circles-font-size='14'
                  />
                  <h4 className='h6 g-font-weight-300'>Kinh tế chính trị</h4>
                </div>
                {/* <!-- End Counter Pie Chart --> */}
              </div>
              {/* <!-- End User Skills --> */}
            </div>
          </div>
        </div>
        {/* <!-- End User Block --> */}

        {/* <!-- Experience Timeline --> */}
        <div className='card border-0 rounded-0 g-mb-40'>
          {/* <!-- Panel Header --> */}
          <div className='card-header d-flex align-items-center justify-content-between g-bg-gray-light-v5 border-0 g-mb-15'>
            <h3 className='h6 mb-0'>
              <i className='icon-briefcase g-pos-rel g-top-1 g-mr-5' /> Các môn đã đăng kí thành công
            </h3>
          </div>
          {/* <!-- End Panel Header --> */}

          {/* <!-- Panel Body --> */}
          <div className='card-block u-info-v1-1 g-bg-white-gradient-v1--after g-height-300 g-pa-0'>
            <ul className='row u-timeline-v2-wrap list-unstyled'>
              <li className='col-md-12 g-brd-bottom g-brd-0--md g-brd-gray-light-v4 g-pb-30 g-pb-0--md g-mb-30 g-mb-0--md'>
                <div className='row'>
                  {/* <!-- Timeline Date --> */}
                  <div className='col-md-3 align-self-center text-md-right g-pr-40--md g-mb-20 g-mb-0--md'>
                    <h4 className='h5 g-font-weight-300'>Nhập môn Công nghệ phần mềm</h4>
                    <h5 className='h6 g-font-weight-300 mb-0'>Thứ 3 - 123</h5>
                  </div>
                  {/* <!-- End Timeline Date --> */}

                  {/* <!-- Timeline Content --> */}
                  <div className='col-md-9 align-self-center g-orientation-left g-pl-40--md'>
                    {/* <!-- Timeline Dot --> */}
                    <div className='g-hidden-sm-down u-timeline-v2__icon g-top-35'>
                      <i className='d-block g-width-18 g-height-18 g-bg-blue g-brd-around g-brd-4 g-brd-gray-light-v4 rounded-circle' />
                    </div>
                    {/* <!-- End Timeline Dot --> */}

                    <article className='g-pa-10--md'>
                      <h3 className='h4 g-font-weight-300'>SE104.K21</h3>
                      <p className='mb-0'>
                            Giảng viên: Đỗ Thị Thanh Tuyền
                      </p>
                    </article>
                  </div>
                  {/* <!-- End Timeline Content --> */}
                </div>
              </li>
              <li className='col-md-12 g-brd-bottom g-brd-0--md g-brd-gray-light-v4 g-pb-30 g-pb-0--md g-mb-30 g-mb-0--md'>
                <div className='row'>
                  {/* <!-- Timeline Date --> */}
                  <div className='col-md-3 align-self-center text-md-right g-pr-40--md g-mb-20 g-mb-0--md'>
                    <h4 className='h5 g-font-weight-300'>Hệ điều hành</h4>
                    <h5 className='h6 g-font-weight-300 mb-0'>Thứ 6 - 345</h5>
                  </div>
                  {/* <!-- End Timeline Date --> */}

                  {/* <!-- Timeline Content --> */}
                  <div className='col-md-9 align-self-center g-orientation-left g-pl-40--md'>
                    {/* <!-- Timeline Dot --> */}
                    <div className='g-hidden-sm-down u-timeline-v2__icon g-top-35'>
                      <i className='d-block g-width-18 g-height-18 g-bg-blue g-brd-around g-brd-4 g-brd-gray-light-v4 rounded-circle' />
                    </div>
                    {/* <!-- End Timeline Dot --> */}

                    <article className='g-pa-10--md'>
                      <h3 className='h4 g-font-weight-300'>IT007.K25</h3>
                      <p className='mb-0'>
                          Giảng viên: Chung Quang Khánh
                      </p>
                    </article>
                  </div>
                  {/* <!-- End Timeline Content --> */}
                </div>
              </li>
              <li className='col-md-12 g-brd-bottom g-brd-0--md g-brd-gray-light-v4 g-pb-30 g-pb-0--md g-mb-30 g-mb-0--md'>
                <div className='row'>
                  {/* <!-- Timeline Date --> */}
                  <div className='col-md-3 align-self-center text-md-right g-pr-40--md g-mb-20 g-mb-0--md'>
                    <h4 className='h5 g-font-weight-300'>Phương pháp mô hình hóa</h4>
                    <h5 className='h6 g-font-weight-300 mb-0'>Thứ 4 - 123</h5>
                  </div>
                  {/* <!-- End Timeline Date --> */}

                  {/* <!-- Timeline Content --> */}
                  <div className='col-md-9 align-self-center g-orientation-left g-pl-40--md'>
                    {/* <!-- Timeline Dot --> */}
                    <div className='g-hidden-sm-down u-timeline-v2__icon g-top-35'>
                      <i className='d-block g-width-18 g-height-18 g-bg-blue g-brd-around g-brd-4 g-brd-gray-light-v4 rounded-circle' />
                    </div>
                    {/* <!-- End Timeline Dot --> */}

                    <article className='g-pa-10--md'>
                      <h3 className='h4 g-font-weight-300'>SE101.K21</h3>
                      <p className='mb-0'>
                          Giảng viên: Lê Thanh Trọng
                      </p>
                    </article>
                  </div>
                  {/* <!-- End Timeline Content --> */}
                </div>
              </li>
              <li className='col-md-12'>
                <div className='row'>
                  {/* <!-- Timeline Date --> */}
                  <div className='col-md-3 align-self-center text-md-right g-pr-40--md g-mb-20 g-mb-0--md'>
                    <h4 className='h5 g-font-weight-300'>Pháp luật đại cương</h4>
                    <h5 className='h6 g-font-weight-300 mb-0'>Thứ 5 - 123</h5>
                  </div>
                  {/* <!-- End Timeline Date --> */}

                  {/* <!-- Timeline Content --> */}
                  <div className='col-md-9 align-self-center g-orientation-left g-pl-40--md'>
                    {/* <!-- Timeline Dot --> */}
                    <div className='g-hidden-sm-down u-timeline-v2__icon g-top-35'>
                      <i className='d-block g-width-18 g-height-18 g-bg-blue g-brd-around g-brd-4 g-brd-gray-light-v4 rounded-circle' />
                    </div>
                    {/* <!-- End Timeline Dot --> */}

                    <article className='g-pa-10--md'>
                      <h3 className='h4 g-font-weight-300'>SS006.K21</h3>
                      <p className='mb-0'>
                            Giảng viên: Huỳnh Thị Nam Hải
                      </p>
                    </article>
                  </div>
                  {/* <!-- End Timeline Content --> */}
                </div>
              </li>
            </ul>
          </div>
          {/* <!-- End Panel Body --> */}
        </div>
        {/* <!-- End Experience Timeline --> */}

      </div>
      {/* <!-- End Profle Content --> */}
    </>
  )
}

export default Profile
