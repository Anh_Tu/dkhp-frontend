import React from 'react'

const PricingInfo = () => {
  return (
    <>
      {/* Section */}
      <section className='container g-pt-100 g-pb-40'>
        <div className='row'>
          <div className='col-lg-6 align-self-center g-mb-60'>
            <img
              className='img-fluid' src='https://i.imgur.com/4o1obwD.jpg' alt='Image Description'
              style={{ borderRadius: 100, wdisplay: 'block', maxWidth: '400px', maxHeight: '400px', width: 'auto', height: 'auto' }}
            />
          </div>
          <div className='col-lg-6 align-self-center g-mb-60 center'>
            <div className='mb-2'>
              <h2 className='h3 g-color-black text-uppercase mb-2'>THÔNG TIN HỌC PHÍ HỌC KỲ 2</h2>
              <div className='d-inline-block g-width-35 g-height-2 g-bg-blue mb-2' />
            </div>
            <div className='mb-5'>
              <p className='mb-5'>
              Hiện chưa có ghi chú nào
              </p>
              <ul className='list-unstyled g-font-size-13 mb-0'>
                <li>
                  <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Số TC học phí: 20.0 (TC)
                </li>
                <li>
                  <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí phải đóng: 4.929.000 (đ)
                </li>
                <li>
                  <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí nợ trước: 0 (đ)
                </li>
                <li>
                  <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí đã đóng: 5.300.000 (đ)
                </li>
                <li>
                  <i className='d-inline-block g-color-primary mr-2 mb-3 fa fa-check' />
                  Học phí còn nợ: -371.000 (đ)
                </li>
              </ul>
            </div>
            {/* <a className='btn btn-md u-btn-outline-black g-font-size-default g-rounded-50 g-py-10 mr-2' href='#'>Explore more</a> */}
            <a className='btn btn-md u-btn-yellow g-font-size-default g-rounded-50 g-py-10' href='#'>Đóng học phí ngay</a>
          </div>
        </div>
      </section>
      {/* End Section */}
    </>
  )
}

export default PricingInfo
