import React from 'react'

const SettingProfile = () => {
  return (
    <>
      {/* Profle Content */}
      <div className='col-lg-9'>
        {/* Nav tabs */}
        <ul className='nav nav-justified u-nav-v1-1 u-nav-blue g-brd-bottom--md g-brd-bottom-2 g-brd-primary g-mb-20' role='tablist' data-target='nav-1-1-default-hor-left-underline' data-tabs-mobile-type='slide-up-down' data-btn-classes='btn btn-md btn-block rounded-0 u-btn-outline-blue g-mb-20'>
          <li className='nav-item'>
            <a className='nav-link g-py-10 active' data-toggle='tab' href='#nav-1-1-default-hor-left-underline--1' role='tab'>Chỉnh sửa thông tin</a>
          </li>
        </ul>
        {/* End Nav tabs */}
        {/* Tab panes */}
        <div id='nav-1-1-default-hor-left-underline' className='tab-content'>
          {/* Edit Profile */}
          <div className='tab-pane fade show active' id='nav-1-1-default-hor-left-underline--1' role='tabpanel' data-parent='#nav-1-1-default-hor-left-underline'>
            {/* <h2 className='h4 g-font-weight-300'>Manage your Name, ID and Email Addresses</h2>
            <p>Below are name, email addresse, contacts and more on file for your account.</p> */}
            <ul className='list-unstyled g-mb-30'>
              {/* Name */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Họ và tên</strong>
                  <span className='align-top'>Nguyễn Trần Hữu Đăng</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Name */}
              {/* Your ID */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Mã số sinh viên</strong>
                  <span className='align-top'>18520014</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Your ID */}
              {/* Company Name */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Khoa</strong>
                  <span className='align-top'>Công nghệ phần mềm</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Company Name */}
              {/* Position */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Lớp</strong>
                  <span className='align-top'>Kĩ thuật phần mềm</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Position */}
              {/* Primary Email Address */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Email</strong>
                  <span className='align-top'>huudang@tungtung.vn</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Primary Email Address */}

              {/* Phone Number */}
              <li className='d-flex align-items-center justify-content-between g-brd-bottom g-brd-gray-light-v4 g-py-15'>
                <div className='g-pr-10'>
                  <strong className='d-block d-md-inline-block g-color-gray-dark-v2 g-width-200 g-pr-10'>Số điện thoại</strong>
                  <span className='align-top'>0386968950</span>
                </div>
                <span>
                  <i className='icon-pencil g-color-gray-dark-v5 g-color-primary--hover g-cursor-pointer g-pos-rel g-top-1' />
                </span>
              </li>
              {/* End Phone Number */}

            </ul>
            <div className='text-sm-right'>
              <a className='btn u-btn-darkgray rounded-0 g-py-12 g-px-25 g-mr-10' href='#'>Huỷ</a>
              <a className='btn u-btn-primary rounded-0 g-py-12 g-px-25' href='#'>Lưu</a>
            </div>
          </div>
          {/* End Edit Profile */}
        </div>
        {/* End Tab panes */}
      </div>
      {/* End Profle Content */}
    </>
  )
}

export default SettingProfile
